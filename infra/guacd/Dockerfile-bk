ARG DOWNLOADER_IMAGE=quay.io/flysangel/alpine:base-v1.0.2
ARG BUILDER_IMAGE=quay.io/flysangel/library/ubuntu:22.04

FROM ${DOWNLOADER_IMAGE} AS downloader

ARG GUACAMOLE_VERSION=1.5.5

USER root

RUN \
    wget https://dlcdn.apache.org/guacamole/${GUACAMOLE_VERSION}/source/guacamole-server-${GUACAMOLE_VERSION}.tar.gz && \
    tar xvfz guacamole-server-${GUACAMOLE_VERSION}.tar.gz

# ---
FROM ${BUILDER_IMAGE} AS builder

ARG GUACAMOLE_VERSION=1.5.5
ARG PREFIX_DIR=/usr/local/guacamole
ARG BUILD_DIR=/tmp/guacamole 

USER root

COPY --from=downloader /guacamole-server-${GUACAMOLE_VERSION} ${BUILD_DIR}

# https://guacamole.apache.org/doc/gug/installing-guacamole.html
# https://github.com/apache/guacamole-server/blob/edce11fcb4ee08792df23fc6c82daf5a502dc30d/Dockerfile
RUN \
    apt update --fix-missing && \
    apt install -y \
    build-essential \
    libcairo2-dev libjpeg-turbo8-dev libpng-dev libtool-bin uuid-dev libossp-uuid-dev \
    libavcodec-dev libavformat-dev libavutil-dev libswscale-dev \
    freerdp2-dev libpango1.0-dev libssh2-1-dev libtelnet-dev libvncserver-dev \
    libwebsockets-dev libpulse-dev libssl-dev libvorbis-dev libwebp-dev && \
    apt autoremove -y && \
    apt clean -y && \
    rm -rf /var/cache/apt/archives

RUN \
    cd ${BUILD_DIR} && \
    autoreconf -fi && \
    ./configure --prefix=${PREFIX_DIR} && \
    make && \
    make install && \
    ldconfig

# https://github.com/apache/guacamole-server/blob/edce11fcb4ee08792df23fc6c82daf5a502dc30d/src/guacd-docker/bin/list-dependencies.sh
RUN \
    lists="${PREFIX_DIR}/sbin/guacd ${PREFIX_DIR}/lib/libguac-client-*.so ${PREFIX_DIR}/lib/freerdp2/*guac*.so" && \
    ls ${lists} | xargs ldd | grep -v 'libguac' | awk '/=>/{print $(NF-1)}' | \
    xargs -I {} basename "{}" | xargs -I {} dpkg-query -S "*/{}" | \
    cut -d ':' -f 1 | sort -u >${PREFIX_DIR}/DEPENDENCIES

# ---
FROM ${BUILDER_IMAGE} AS main

ENV \
    GUACD_HOME=/usr/local/guacamole \
    LD_LIBRARY_PATH=${GUACD_HOME}/lib \
    GUACD_LOG_LEVEL=info

USER root

COPY --from=builder ${GUACD_HOME} ${GUACD_HOME}

RUN \
    apt update --fix-missing && \
    apt install --no-install-recommends -y \
    wget ca-certificates ghostscript netcat-openbsd \
    # chinese fonts
    ttf-wqy-zenhei \
    # guacd dependencies
    $(cat ${GUACD_HOME}/DEPENDENCIES) && \
    apt autoremove -y && \
    apt clean -y && \
    rm -rf /var/cache/apt/archives

# UID=1000, GID=1000, same with guacamole client
ARG UID=1000
ARG GID=1000

RUN \
    groupadd --gid ${GID} guacd && \
    useradd --system --create-home --shell /usr/sbin/nologin --uid ${UID} --gid ${GID} guacd

RUN \
    mkdir -p /var/lib/guacamole/recordings

RUN \
    wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/system/locale/locale.txt | bash

ENV LANG=en_US.UTF-8

USER guacd

WORKDIR ${GUACD_HOME}

CMD ${GUACD_HOME}/sbin/guacd -b 0.0.0.0 -L ${GUACD_LOG_LEVEL} -f
