#!/bin/bash

#set -x # debug mode
mkdir -p ${GUACAMOLE_HOME}/guacamole-ext
mkdir -p ${GUACAMOLE_HOME}/ext/images

# change
# https://www.flaticon.com/packs/greek-mythology-47
for LOGO in $(ls ${GUACAMOLE_HOME}/images); do
  # prepare
  cp -r ${GUACAMOLE_HOME}/images/${LOGO} ${GUACAMOLE_HOME}/ext/images
  sed -i "s|LOGO|${LOGO}|g" ${GUACAMOLE_HOME}/ext/guac-manifest.json
  sed -i "s|LOGO|${LOGO}|g" ${GUACAMOLE_HOME}/ext/css/login-override.css

  # build ext.jar
  jar cvf ${GUACAMOLE_HOME}/guacamole-ext/${LOGO}.jar -C ${GUACAMOLE_HOME}/ext .

  # reset
  rm -rf ${GUACAMOLE_HOME}/ext/images/${LOGO}
  sed -i "s|${LOGO}|LOGO|g" ${GUACAMOLE_HOME}/ext/guac-manifest.json
  sed -i "s|${LOGO}|LOGO|g" ${GUACAMOLE_HOME}/ext/css/login-override.css
done
